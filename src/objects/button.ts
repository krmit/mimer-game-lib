"use strict";
// Inspired from https://phasergames.com/how-to-make-buttons-in-phaser-3/
// @ts-ignore
import Phaser from "phaser";

interface frameType {
  texture: string;
  frame: number;
}

interface frameConfig {
  texture: Phaser.Textures.Texture;
}

interface buttonConfig {
  scene?: Phaser.Scene;
  key?: string;
  x: number;
  y: number;
  up: frameType | number;
  over?: frameType | number;
  down?: frameType | number;
}

function convertToFrame(
  frameName: "up" | "down" | "over",
  config: buttonConfig
): frameType {
  const frame = config[frameName];
  if (frame !== undefined && typeof frame !== "number") {
    return frame;
  } else if (
    frame !== undefined &&
    typeof config.key === "string" &&
    Number.isInteger(config[frameName])
  ) {
    return { texture: config.key, frame: frame };
  } else {
    throw "Bad config for frame.";
  }
}

export function createButton(scene: Phaser.Scene, config: buttonConfig) {
  config.scene = scene;
  let button = new Button(config);
  scene.children.add(button);
  return button;
}

export default class Button extends Phaser.GameObjects.Sprite {
  up: frameType;
  down: frameType;
  over: frameType;

  constructor(config: buttonConfig) {
    if (!config.scene) {
      throw "Missing scene";
    }

    if (config.up === undefined) {
      throw "Missing image for button!";
    }
    let frame = convertToFrame("up", config);

    super(config.scene, config.x, config.y, frame.texture, frame.frame);
    this.up = frame;

    if (config.down === undefined) {
      this.down = this.up;
    } else {
      this.down = convertToFrame("down", config);
    }

    if (config.over === undefined) {
      this.over = this.up;
    } else {
      this.over = convertToFrame("over", config);
    }

    if (!config.x) {
      config.x = 0;
    }

    if (!config.y) {
      config.y = 0;
    }

    config.scene.add.existing(this);
    this.setInteractive();
    this.on("pointerdown", this.onDown, this);
    this.on("pointerup", this.onOver, this);
    this.on("pointerover", this.onOver, this);
    this.on("pointerout", this.onUp, this);
  }

  onDown() {
    this.setTexture(this.down.texture, this.down.frame);
  }

  onOver() {
    this.setTexture(this.over.texture, this.over.frame);
  }

  onUp() {
    this.setTexture(this.up.texture, this.up.frame);
  }
}
