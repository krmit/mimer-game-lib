// @ts-ignore
import Phaser from "phaser";
import Button from "../objects/button";

export default class ButtonPlugin extends Phaser.Plugins.BasePlugin {
  constructor(pluginManager:any) {
    super(pluginManager);
    // @ts-ignore
    pluginManager.registerGameObject("Button", this.createButton);
  }
}
