
import Keypad from "../objects/keypad";

export default class KeypadPlugin extends Phaser.Plugins.BasePlugin {
  constructor(pluginManager:any) {
    super(pluginManager);
    pluginManager.registerGameObject("Keypad", this.createKeypad);
  }

  createKeypad(config:any) {
    // @ts-ignore
    config.scene = this.scene;
    let keypad = new Keypad(config);
    // @ts-ignore
    let result = this.displayList.add(keypad);
    return keypad;
  }
}
