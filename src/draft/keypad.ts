"use strict";
// Inspired from https://phasergames.com/how-to-make-buttons-in-phaser-3/
import Block from "../block/block.js";
import Button from "../objects/button.js";

export default class Keypad extends Block {
  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      console.error(config.key + "Missing key for buttons");
      return;
    }

    if (!config.paddingLeft) {
      config.paddingLeft = 0;
    }

    if (!config.paddingTop) {
      config.paddingTop = 0;
    }

    if (!config.buttonScale) {
      config.buttonScale = 1;
    }

    if (!config.buttonSize) {
      config.buttonSize = 128;
    }

    if (!config.buttonScale) {
      config.buttonScale = 1;
    }

    if (!config.spacing) {
      config.spacing = 4;
    }

    super(config);
    const button_with = config.spacing + config.buttonSize;
    const padding_left = this.x + config.paddingLeft;
    const padding_top = this.y + config.paddingTop;

    let zero = new Button({
      scene: config.scene,
      key: config.key,
      up: 0,
      over: 10,
      down: 20,
      x: padding_left + button_with,
      y: padding_top + 3 * button_with
    });
    zero.setScale(config.buttonScale);
    let _this = this;
    zero.on("pointerdown", () => _this.emit("pointerdown", 0), this);

    this.add(zero);

    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        let button = new Button({
          scene: config.scene,
          key: config.key,
          up: 1 + i * 3 + j,
          over: 11 + i * 3 + j,
          down: 21 + i * 3 + j,
          x: padding_left + j * button_with,
          y: padding_top + i * button_with
        });
        this.add(button);
        button.setScale(config.buttonScale);
        button.on(
          "pointerdown",
          () => _this.emit("pointerdown", j + i * 3 + 1),
          this
        );
      }
    }
  }
}
