"use strict";
// Inspired from https://phasergames.com/ui-blocks-a-lightweight-alternate-to-containers/
import Phaser from "phaser";

// Todo: This should extend Phaser.GameObjects.GameObject
export default class Block extends Phaser.GameObjects.Sprite {
  constructor(config:any) {
    if (!config.scene) {
      console.error("Missing scene");
      return;
    }

    // Position
    if (!config.x) {
      config.x = 0;
    }

    if (!config.y) {
      config.y = 0;
    }

    super(config.scene, config.x, config.y, null);
    this.visible = false;
    config.scene.add.existing(this);

    this._x = config.x;
    this._y = config.y;

    this._pre_x = 0;
    this._pre_y = 0;

    // Children as an array
    this.children = [];
  }

  //Add a child
  add(child) {
    child.childIndex = this.children.length;
    this.children.push(child);
  }

  // Setters for position
  set x(val) {
    // Put x value in tmp variable
    this._pre_x = this._x;

    // Set new value
    this._x = val;

    // Update the children
    this._updatePositions();
  }

  set y(val) {
    this._pre_y = this._y;
    this._y = val;
    this._updatePositions();
  }

  // Getters for position
  get x() {
    return this._x;
  }
  get y() {
    return this._y;
  }

  setXY(x, y) {
    this._x = x;
    this._y = y;
    this._updatePositions();
  }

  removeChild(child) {
    // Take the child off the array based on index
    this.children.splice(child.childIndex, 1);

    // Rebuild the indexes
    const len = this.children.length;
    for (let i = 0; i < len; i++) {
      this.children[i].childIndex = i;
    }
    // Set the childIndex to the length of the array
    this.childIndex = len;
  }

  getRelPos(child) {
    return {
      x: child.x - this._x,
      y: child.y - this._y
    };
  }

  _updateChildPos(child) {
    child.x = child.x - this._pre_x + this._x;
    child.y = child.y - this._pre_y + this._y;
  }

  _updatePositions() {
    if (this.children === null && this.children.length > 0) {
      // Rebuild the indexes
      const len = this.children.length;
      for (let i = 0; i < len; i++) {
        this._updateChildPos(this.children[i]);
      }
    }
    // Update values
    this._pre_x = this._x;
    this._pre_y = this._y;
  }
}
