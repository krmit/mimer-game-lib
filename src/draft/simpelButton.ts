"use strict";
// Inspired from https://phasergames.com/how-to-make-buttons-in-phaser-3/
import CSSButton from "./cssButton";

export function createSimpelButton(game: any, config: any) {
  config.scene = game.scene;
  let button = new Button(config);
  let result = game.displayList.add(button);
  return button;
}

export default class SimpelButton extends CSSButton {
  config: any;

  constructor(config: any, type: string, title: string) {
    if (!title && !config.title) {
      console.error("missing key!");
      return;
    }

    switch (type) {
      case "red":
        config.cssUp = {
          border: "5px"
        };
        break;
      default:
        config.cssUp = {
          border: "5px"
        };
    }

    super(config);
  }
}
