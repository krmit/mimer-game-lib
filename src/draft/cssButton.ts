"use strict";
// Inspired from https://phasergames.com/how-to-make-buttons-in-phaser-3/
import Button from "./button";

export function createCSSButton(game: any, config: any) {
  config.scene = game.scene;
  let button = new Button(config);
  let result = game.displayList.add(button);
  return button;
}

export default class CSSButton extends Button {
  config: any;

  constructor(config: any) {
    if (!config.scene) {
      console.error("missing scene");
      return;
    }

    if (!config.key) {
      console.error("missing key!");
      return;
    }

    if (!config.title) {
      console.error("missing key!");
      return;
    }

    if (!config.cssUp) {
      config.cssUp = {
        border: "5px"
      };
    }
    config.up = this.add.dom(400, 300, "div", config.cssUp, config.title);

    if (!config.cssDown) {
      config.down = config.up;
    }

    if (!config.cssOver) {
      config.over = config.up;
    }

    super(config);
  }
}
