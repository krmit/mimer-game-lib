export { AboutScene } from "./about.js";
export { CreditsScene } from "./credit.js";
export { MenuScene } from "./menu.js";
export { ResultScene } from "./result.js";
export { TitleScene } from "./title.js";
export { HighScoreScene } from "./high-score";
