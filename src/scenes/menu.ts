import Phaser from "phaser";
import { createButton } from "../objects/button.js";
const assetsPath = "../mimer-assets-pre/";

export class MenuScene extends Phaser.Scene {
  config: any;
  left: number;
  top: number;
  color: string;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "menuScene";
    }

    if (!config.startScene) {
      throw config.key + ": Missing next scene";
    }

    if (!config.titleText) {
      throw config.key + ": Missing text for title";
    }

    if (!config.time) {
      config.time = 1000;
    }

    super(config);
    this.config = config;
    this.left = 200;
    this.top = 225;
    this.color = "red";
  }

  preload() {
    this.load.spritesheet("startButton", "assets/start-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });

    this.load.spritesheet("aboutButton", "assets/about-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });

    this.load.spritesheet("creditButton", "assets/credit-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });
    this.load.spritesheet(
      "highScoreButton",
      "assets/high-score-100x50-3x1.png",
      { frameWidth: 100, frameHeight: 50 }
    );
  }

  create() {
    console.log("Create Menu scene with title " + this.config.titleText);
    this.add.text(this.left, 100, this.config.titleText, {
      fontFamily: "Arial",
      // @ts-ignore
      fontSize: 64,
      color: this.color
    });

    let start = createButton(this, {
      key: "startButton",
      x: 175 + this.left,
      y: this.top,
      up: 0,
      over: 1,
      down: 2
    });

    start.on("pointerdown", this.onStart, this);

    let about = createButton(this, {
      key: "aboutButton",
      x: 175 + this.left,
      y: this.top + 55,
      up: 0,
      over: 1,
      down: 2
    });

    about.on("pointerdown", this.onAbout, this);

    let high_score = createButton(this, {
      key: "highScoreButton",
      x: 175 + this.left,
      y: this.top + 2 * 55,
      up: 0,
      over: 1,
      down: 2
    });

    high_score.on("pointerdown", this.onHighScore, this);

    let credit = createButton(this, {
      key: "creditButton",
      x: 175 + this.left,
      y: this.top + 3 * 55,
      up: 0,
      over: 1,
      down: 2
    });

    credit.on("pointerdown", this.onCredit, this);
  }

  onStart() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.startScene);
  }

  onAbout() {
    console.log("Start scene aboutScene");
    this.scene.start("aboutScene");
  }

  onHighScore() {
    console.log("Start scene highScoreScene");
    this.scene.start("highScoreScene");
  }

  onCredit() {
    console.log("Start scene crdeditScene");
    this.scene.start("creditScene");
  }
}
