import Phaser from "phaser";
import { createButton } from "../objects/button.js";

export class ResultScene extends Phaser.Scene {
  config: any;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "resultScene";
    }

    if (!config.nextScene) {
      throw config.key + ": Missing next scene";
    }

    if (!config.resultText) {
      config.resultText = "Result";
    }

    if (!config.time) {
      config.time = 3000;
    }

    super(config);
    this.config = config;
  }

  create() {
    console.log("CreateResuilt scene");
    this.add.text(25, 100, this.config.resultText, {
      fontFamily: "Arial",
      fontSize: "64",
      color: "#ffffff"
    });

    let ok = createButton(this, {
      key: "okButton",
      x: 80,
      y: 400,
      up: 0,
      over: 1,
      down: 2
    });

    ok.on("pointerdown", this.onOk, this);
  }

  onOk() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}
