import { createButton } from "../objects/button.js";
import Phaser from "phaser";

export class AboutScene extends Phaser.Scene {
  config: any;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "aboutScene";
    }

    if (!config.nextScene) {
      throw config.key + ": Missing next scene";
    }

    if (!config.time) {
      config.time = 5000;
    }

    super(config);
    this.config = config;
  }

  create() {
    this.add.text(25, 100, "About", {
      fontFamily: "Arial",
      // @ts-ignore
      fontSize: 64,
      color: "#ffffff"
    });
    let ok = createButton(this, {
      key: "okButton",
      x: 80,
      y: 400,
      up: 0,
      over: 1,
      down: 2
    });

    ok.on("pointerdown", this.onOk, this);
  }

  onOk() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}
