import Phaser from "phaser";

export default class Countdown extends Phaser.Scene {
  config: any;
  clock: any;
  timer: any;
  count: number;
  timerEnd: any;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "countdownScene";
    }

    if (!config.count) {
      config.count = 9;
    }

    if (!config.nextScene) {
      config.nextScene = null;
    }

    super(config);

    this.clock = null;
    this.timer = null;
    this.count = config.count;
    this.config = config;
  }

  create() {
    this.clock = this.add.text(25, 100, String(this.count), {
      fontFamily: "Arial",
      fontSize: "512",
      color: "#ff0000"
    });

    this.timer = this.time.addEvent({
      delay: 1000,
      callback: this.onTick,
      repeat: this.count - 1,
      callbackScope: this
    });

    this.timerEnd = this.time.addEvent({
      delay: 1000 * (this.count + 1),
      callback: this.onFinish,
      callbackScope: this
    });
  }

  onTick() {
    this.count--;
    this.clock.setText(this.count);
  }

  onFinish() {
    this.count = this.config.count;
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}
