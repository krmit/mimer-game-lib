import Phaser from "phaser";
import {
  TitleScene,
  MenuScene,
  CreditsScene,
  AboutScene,
  HighScoreScene
} from "../src/scenes/game";

class LoadScene extends Phaser.Scene {
  config: any;

  constructor(config: any) {
    super(config);
    this.config = config;
    Phaser.Scene.call(this, { key: "loadScene", active: true });
  }

  preload() {
    this.load.spritesheet("buttons", "assets/ok-50x50-3x1.png", {
      frameWidth: 50,
      frameHeight: 50
    });
    this.load.spritesheet("startButton", "assets/start-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });
    this.load.spritesheet("aboutButton", "assets/about-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });
    this.load.spritesheet("creditButton", "assets/credit-100x50-3x1.png", {
      frameWidth: 100,
      frameHeight: 50
    });
    this.load.spritesheet(
      "highScoreButton",
      "assets/high-score-100x50-3x1.png",
      { frameWidth: 100, frameHeight: 50 }
    );
    this.load.spritesheet("okButton", "assets/ok-50x50-3x1.png", {
      frameWidth: 50,
      frameHeight: 50
    });
  }

  create() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}

let title_scene = new TitleScene({
  time: 1000,
  titleText: "Gods and Trolls",
  nextScene: "menuScene"
});

let menu_scene = new MenuScene({
  titleText: "Gods and trolls",
  startScene: "aboutScene"
});

let highscore_scene = new HighScoreScene({
  resultText: "Result!",
  nextScene: "menuScene"
});

let credits_scene = new CreditsScene({
  resultText: "Result!",
  nextScene: "menuScene"
});

let about_scene = new AboutScene({
  nextScene: "menuScene"
});

let load_scene = new LoadScene({
  nextScene: "titleScene"
});

const config = {
  type: Phaser.WEBGL,
  backgroundColor: "#000",
  scale: {
    mode: Phaser.Scale.FIT,
    parent: "game",
    width: 800,
    height: 600
  },
  parent: "game",
  scene: [
    load_scene,
    highscore_scene,
    title_scene,
    menu_scene,
    credits_scene,
    about_scene
  ]
};

window.onload = function() {
  console.log("Start game");
  const game = new Phaser.Game(config);
};
